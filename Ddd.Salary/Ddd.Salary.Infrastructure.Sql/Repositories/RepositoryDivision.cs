﻿using Ddd.Salary.Domain.Entities;
using Ddd.Salary.Domain.Interfaces;
using Ddd.Salary.Domain.Repositories;
using Ddd.Salary.Infrastructure.Sql.Data;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Ddd.Salary.Infrastructure.Sql.Repositories
{
    public class RepositoryDivision : IRepositoryDivision<Division, int, string>
    {
        private readonly ApplicationDbContext _context;

        public RepositoryDivision(ApplicationDbContext context)
        {
            this._context = context;
        }

        public List<Division> List()
        {
            var divisions = this._context.Division.ToList();
            return divisions;
        }

        public Division ListById(int entityPK)
        {
            throw new NotImplementedException();
        }

        public List<Division> ListMultipleByID(string entityID)
        {
            throw new NotImplementedException();
        }
    }
}
