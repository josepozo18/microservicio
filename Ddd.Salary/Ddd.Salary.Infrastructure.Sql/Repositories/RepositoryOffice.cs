﻿using Ddd.Salary.Domain.Entities;
using Ddd.Salary.Domain.Repositories;
using Ddd.Salary.Infrastructure.Sql.Data;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Ddd.Salary.Infrastructure.Sql.Repositories
{
    public class RepositoryOffice : IRepositoryOffice<Office, int, string>
    {
        private readonly ApplicationDbContext _context;

        public RepositoryOffice(ApplicationDbContext context)
        {
            this._context = context;
        }

        public List<Office> List()
        {
            var offices = this._context.Offices.ToList();
            return offices;
        }

        public Office ListById(int entityPK)
        {
            throw new NotImplementedException();
        }

        public List<Office> ListMultipleByID(int entityID)
        {
            throw new NotImplementedException();
        }

        public List<Office> ListMultipleByID(string entityID)
        {
            throw new NotImplementedException();
        }
    }
}
