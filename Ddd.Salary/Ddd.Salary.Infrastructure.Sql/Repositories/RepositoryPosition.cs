﻿using Ddd.Salary.Domain.Entities;
using Ddd.Salary.Domain.Repositories;
using Ddd.Salary.Infrastructure.Sql.Data;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Ddd.Salary.Infrastructure.Sql.Repositories
{
    public class RepositoryPosition : IRepositoryPosition<Position, int, string>
    {
        private readonly ApplicationDbContext _context;

        public RepositoryPosition(ApplicationDbContext context)
        {
            this._context = context;
        }
        public List<Position> List()
        {
            var positions = this._context.Position.ToList();
            return positions;
        }

        public Position ListById(int entityPK)
        {
            throw new NotImplementedException();
        }

        public List<Position> ListMultipleByID(int entityID)
        {
            throw new NotImplementedException();
        }

        public List<Position> ListMultipleByID(string entityID)
        {
            throw new NotImplementedException();
        }
    }
}
