﻿
using System.Collections.Generic;
using System.Linq;
using Ddd.Salary.Domain.Entities;
using Ddd.Salary.Domain.Helpers;
using Ddd.Salary.Domain.Repositories;
using Ddd.Salary.Infrastructure.Sql.Data;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;

namespace Ddd.Salary.Infrastructure.Sql.Repositories
{
    public class RepositorySalary : IRepositorySalary<SalaryAux<_Salary>, _Salary, int, string>
    {
        private readonly ApplicationDbContext _context;

        public RepositorySalary(ApplicationDbContext context)
        {
            this._context = context;
        }

        public SalaryAux<_Salary> Add(List<_Salary> entity)
        {
            SalaryAux<_Salary> salaryAux = new SalaryAux<_Salary>();
            foreach (var item in entity)
            {
                var employeeExists = this._context.Salaries
                     .Where(x => x.Name == item.Name && x.Surname == item.Surname && x.EmployeeCode != item.EmployeeCode)
                     .ToList();

                if (employeeExists.Count > 0)
                {
                    salaryAux.Data = employeeExists;
                    salaryAux.Message = string.Format("El empleado {0} {1} ya existe. Duplicidad no valida!", item.Name, item.Surname);
                    return salaryAux;
                }

                var resultExist = this._context.Salaries
                    .Where(x => x.Id != item.Id && x.EmployeeCode == item.EmployeeCode && x.Month == item.Month && x.Year == item.Year).ToList();
                if (resultExist.Count > 0)
                {
                    salaryAux.Data = resultExist;
                    salaryAux.Message = string.Format("El empleado {0} {1} ya tiene registrado el mes {2} para el año {3}", item.Name, item.Surname, item.Month, item.Year);
                    return salaryAux;
                }
            }
            this._context.Salaries.AddRange(entity);

            if (salaryAux.Data.Count > 0)
            {
                salaryAux.Message = "Registros guardados correctamente.";
            }

            return salaryAux;
        }

        public SalaryAux<_Salary> Edit(List<_Salary> entities)
        {
            SalaryAux<_Salary> salaryAux = new SalaryAux<_Salary>();
            foreach(var entity in entities)
            {

                var employeeExists = this._context.Salaries
                    .Where(x => x.Name == entity.Name && x.Surname == entity.Surname && x.EmployeeCode != entity.EmployeeCode)
                    .ToList();

                if (employeeExists.Count > 0)
                {
                    salaryAux.Data = employeeExists;
                    salaryAux.Message = string.Format("El empleado {0} {1} ya existe. Duplicidad no valida!", entity.Name, entity.Surname);
                    return salaryAux;
                }

                var resultExist = this._context.Salaries
                    .Where(x => x.Id != entity.Id && x.EmployeeCode == entity.EmployeeCode && x.Month == entity.Month && x.Year == entity.Year).ToList();
                if (resultExist.Count > 0)
                {
                    salaryAux.Data = resultExist;
                    salaryAux.Message = string.Format("El empleado {0} {1} ya tiene registrado el mes {2} para el año {3}", entity.Name, entity.Surname, entity.Month, entity.Year);
                    return salaryAux;
                }
                
                var salarySelected = this._context.Salaries.Where(x => x.Id == entity.Id).FirstOrDefault();
                if (salarySelected != null)
                {

                    salarySelected.Year = entity.Year;
                    salarySelected.Month = entity.Month;
                    salarySelected.OfficeId = entity.OfficeId;
                    salarySelected.Name = entity.Name;
                    salarySelected.Surname = entity.Surname;
                    salarySelected.DivisionId = entity.DivisionId;
                    salarySelected.PositionId = entity.PositionId;
                    salarySelected.Grade = entity.Grade;
                    salarySelected.BeginDate = entity.BeginDate;
                    salarySelected.BirthDay = entity.BirthDay;
                    salarySelected.IdentificationNumber = entity.IdentificationNumber;
                    salarySelected.BaseSalary = entity.BaseSalary;
                    salarySelected.ProductionBonus = entity.ProductionBonus;
                    salarySelected.CompensationBonus = entity.CompensationBonus;
                    salarySelected.Comission = entity.Comission;
                    salarySelected.Contributions = entity.Contributions;
                    this._context.Entry(salarySelected).State = Microsoft.EntityFrameworkCore.EntityState.Modified;

                    if (salaryAux.Data == null)
                    {
                        salaryAux.Data = new List<_Salary>();
                    }
                    salaryAux.Data.Add(salarySelected);
                }
                else
                {
                    this._context.Entry(entity).State = Microsoft.EntityFrameworkCore.EntityState.Added;

                    if (salaryAux.Data == null)
                    {
                        salaryAux.Data = new List<_Salary>();
                    }
                    salaryAux.Data.Add(entity);
                }
            }

            if (salaryAux.Data.Count > 0)
            {
                salaryAux.Message = "Registros guardados correctamente.";
            }

            return salaryAux;
        }

        public List<_Salary> List()
        {

            // Todos los sueldos
            var allData = this._context.Salaries
                .Include(x => x.Office)
                .Include(x => x.Position)
                .Include(x => x.Division)
                .ToList();

            // Solo ultimo sueldo
            var salaries = new List<_Salary>();
            foreach (var item in allData.GroupBy(x => x.EmployeeCode))
            {
                //var row = (from s in allData where s.EmployeeCode == item.Key select new { s.EmployeeCode, s.Year, s.Month, s.BaseSalary }).ToList();
                var row = (from s in allData where s.EmployeeCode == item.Key select new {s}).ToList();
                var tempo = row.Aggregate((item1, item2) => (item1.s.EmployeeCode == item2.s.EmployeeCode) && (item1.s.Year > item2.s.Year) && (item1.s.Month > item2.s.Month) ? item1 : item2);

                salaries.Add(tempo.s);
            }

            return salaries;
        }

        public _Salary ListById(int entityID)
        {
            var data = this._context.Salaries
                .Include(x => x.Office)
                .Include(x => x.Position)
                .Include(x => x.Division)
                .Where(x => x.Id == entityID)
                .FirstOrDefault();
            return data;
        }

        public List<_Salary> ListMultipleByID(string entityID)
        {
            // Por ahora no es necesario es metodo
            var allData = this._context.Salaries
                .Include(x => x.Office)
                .Include(x => x.Position)
                .Include(x => x.Division)
                .Where(x => x.EmployeeCode == entityID)
                .ToList();
            return allData;
        }

        public void SaveChanges()
        {
            this._context.SaveChanges();
        }
        
    }
}
