﻿using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Ddd.Salary.Domain.Entities;
using Microsoft.Extensions.Configuration;

namespace Ddd.Salary.Infrastructure.Sql.Data
{
    public class ApplicationDbContext: DbContext
    {
        public ApplicationDbContext(DbContextOptions<ApplicationDbContext> options)
            : base(options)
        {
        }

        public DbSet<_Salary> Salaries { get; set; }
        public DbSet<Office> Offices { get; set; }
        public DbSet<Division> Division { get; set; }
        public DbSet<Position> Position { get; set; }
    }
}
