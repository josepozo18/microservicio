﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace Ddd.Salary.Infrastructure.Sql.Migrations
{
    public partial class Field_Birhtday : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.RenameColumn(
                name: "Date",
                schema: "dbo",
                table: "Salary",
                newName: "BirthDay");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.RenameColumn(
                name: "BirthDay",
                schema: "dbo",
                table: "Salary",
                newName: "Date");
        }
    }
}
