﻿using AutoMapper;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Ddd.Salary.Domain.Entities;
using Ddd.Salary.WebShared.Dtos;

namespace Ddd.Salary.Api.Profiles
{
    public class MappingProfiles: Profile
    {
        public MappingProfiles()
        {
            CreateMap<Position, PositionDto>().ReverseMap();

            CreateMap<Office, OfficeDto>().ReverseMap();

            CreateMap<Division, DivisionDto>().ReverseMap();

            CreateMap<_Salary, SalaryDto>()
                .ForMember(x => x.Office, option => option.MapFrom(MapSalaryOffice))
                .ForMember(x => x.Division, option => option.MapFrom(MapSalaryDivision))
                .ForMember(x => x.Position, option => option.MapFrom(MapSalaryPosition))
                .ForMember(x => x.TotalSalary, option => option.MapFrom(MapTotalSalary))
                .ReverseMap();
            
            CreateMap<SalaryCreationDto, _Salary>();
            CreateMap<SalaryModificationDto, _Salary>();
        }

        private decimal MapTotalSalary(_Salary salary, SalaryDto salaryDto)
        {
            salaryDto.TotalSalary = 0;
            var otherIncome = ((salary.BaseSalary + salary.Comission) * (decimal)0.08) + salary.Comission;
            var totalSalary = (salary.BaseSalary + salary.ProductionBonus + (salary.CompensationBonus * (decimal)0.75) + otherIncome) - salary.Contributions;
            return totalSalary;
        }

        private OfficeDto MapSalaryOffice(_Salary salary, SalaryDto salaryDto)
        {
            var result = new OfficeDto();
            if (salary.Office == null) return result;
            result.Id = salary.OfficeId;
            result.Nombre = salary.Office.Nombre;
            return result;
        }

        private DivisionDto MapSalaryDivision(_Salary salary, SalaryDto salaryDto)
        {
            var result = new DivisionDto();
            if (salary.Division == null) return result;
            result.Id = salary.DivisionId;
            result.Nombre = salary.Division.Nombre;
            return result;
        }
        
        private PositionDto MapSalaryPosition(_Salary salary, SalaryDto salaryDto)
        {
            var result = new PositionDto();
            if (salary.Position == null) return result;
            result.Id = salary.PositionId;
            result.Nombre = salary.Position.Nombre;
            return result;
        }

    }
}
