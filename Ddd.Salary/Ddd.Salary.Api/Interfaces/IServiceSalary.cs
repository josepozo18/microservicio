﻿using Ddd.Salary.Domain.Interfaces;

namespace Ddd.Salary.Api.Interfaces
{
    public interface IServiceSalary<TEntityAux, TEntity, TEntityPK, TEntityID>
        : IAdd<TEntityAux, TEntity>, IEdit<TEntityAux, TEntity>, IList<TEntity, TEntityPK, TEntityID>
    {
    }
}
