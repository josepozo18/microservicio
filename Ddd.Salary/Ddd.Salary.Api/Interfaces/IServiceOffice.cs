﻿using Ddd.Salary.Domain.Interfaces;

namespace Ddd.Salary.Api.Interfaces
{
    public interface IServiceOffice<TEntidad, TEntidyPK, TEntidadID>
        : IList<TEntidad, TEntidyPK, TEntidadID>
    {
    }
}
