﻿using Ddd.Salary.Domain.Interfaces;

namespace Ddd.Salary.Api.Interfaces
{
    public interface IServiceDivision<TEntidad, TEntidyPK, TEntidadID>
        : IList<TEntidad, TEntidyPK, TEntidadID>
    {
    }
}
