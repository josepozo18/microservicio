﻿using AutoMapper;
using Ddd.Salary.Api.Services;
using Ddd.Salary.Infrastructure.Sql.Data;
using Ddd.Salary.Infrastructure.Sql.Repositories;
using Ddd.Salary.WebShared.Dtos;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Ddd.Salary.Api.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class PositionController: ControllerBase
    {
        private readonly IMapper _mapper;
        private readonly ServicePosition _service;
        private readonly RepositoryPosition _repositoryPosition;

        public PositionController(ApplicationDbContext context, IMapper mapper)
        {
            this._mapper = mapper;
            this._repositoryPosition = new RepositoryPosition(context);
            this._service = new ServicePosition(this._repositoryPosition);
        }

        [HttpGet]
        public ActionResult<List<PositionDto>> Get()
        {
            var listPositions = this._service.List();
            var listPositionsDto = this._mapper.Map<List<PositionDto>>(listPositions);
            
            return listPositionsDto;
        }

        //[HttpGet("{Id:int}", Name = "Position_GetById")]
        //public ActionResult<PositionDto> Get([FromRoute] int Id)
        //{
        //    var position = this._service.ListByID(Id);
        //    var positionDto = this._mapper.Map<PositionDto>(position);

        //    return positionDto;
        //}
    }
}
