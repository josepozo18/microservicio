﻿using AutoMapper;
using Ddd.Salary.Api.Services;
using Ddd.Salary.Infrastructure.Sql.Data;
using Ddd.Salary.Infrastructure.Sql.Repositories;
using Ddd.Salary.WebShared.Dtos;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Ddd.Salary.Api.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class OfficeController: ControllerBase
    {
        private readonly IMapper _mapper;
        private readonly ServiceOffice _service;
        private readonly RepositoryOffice _repositoryOffice;

        public OfficeController(ApplicationDbContext context, IMapper mapper)
        {
            this._mapper = mapper;
            this._repositoryOffice = new RepositoryOffice(context);
            this._service = new ServiceOffice(this._repositoryOffice);
        }

        [HttpGet]
        public ActionResult<List<OfficeDto>> Get()
        {
            var listOffices = this._service.List();
            var listOfficesDto = this._mapper.Map<List<OfficeDto>>(listOffices);

            return listOfficesDto;
        }

        //[HttpGet("{Id:int}", Name = "Office_GetById")]
        //public ActionResult<OfficeDto> Get([FromRoute] int Id)
        //{
        //    var office = this._service.ListByID(Id);
        //    var officeDto = this._mapper.Map<OfficeDto>(office);

        //    return officeDto;
        //}
    }
}
