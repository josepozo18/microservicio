﻿using AutoMapper;
using Ddd.Salary.Api.Services;
using Ddd.Salary.Infrastructure.Sql.Data;
using Ddd.Salary.Infrastructure.Sql.Repositories;
using Ddd.Salary.WebShared.Dtos;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Ddd.Salary.Api.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class DivisionController: ControllerBase
    {
        private readonly IMapper _mapper;
        private readonly ServiceDivision _service;
        private readonly RepositoryDivision _repositoryDivision;

        public DivisionController(ApplicationDbContext context, IMapper mapper)
        {
            this._mapper = mapper;
            this._repositoryDivision = new RepositoryDivision(context);
            this._service = new ServiceDivision(this._repositoryDivision);
        }

        [HttpGet]
        public ActionResult<List<DivisionDto>> Get()
        {
            var listDivision = this._service.List();
            var listDivisionDto = this._mapper.Map<List<DivisionDto>>(listDivision);

            return listDivisionDto;
        }

        //[HttpGet("{Id:int}", Name = "Division_GetById")]
        //public ActionResult<DivisionDto> Get([FromRoute] int Id)
        //{
        //    var division = this._service.ListByID(Id);
        //    var divisionDto = this._mapper.Map<DivisionDto>(division);

        //    return divisionDto;
        //}
    }
}
