﻿using Microsoft.AspNetCore.Mvc;

using Ddd.Salary.WebShared.Dtos;
using System.Collections.Generic;
using AutoMapper;
using Ddd.Salary.Api.Services;
using Ddd.Salary.Domain.Entities;
using Ddd.Salary.Infrastructure.Sql.Repositories;
using Ddd.Salary.Infrastructure.Sql.Data;
using Ddd.Salary.Domain.Helpers;

namespace Ddd.Salary.Api.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class SalaryController: ControllerBase
    {

        private readonly IMapper _mapper;
        private readonly ServiceSalary _service;
        private readonly RepositorySalary _repositorySalary;

        public SalaryController(ApplicationDbContext context, IMapper mapper)
        {
            this._mapper = mapper;
            this._repositorySalary = new RepositorySalary(context);
            this._service = new ServiceSalary(this._repositorySalary);
        }


        [HttpGet]
        public ActionResult<List<SalaryDto>> Get()
        {
            var listSalaries = this._service.List();
            var listSalariesDto = this._mapper.Map<List<SalaryDto>>(listSalaries);

            return listSalariesDto;
        }

        [HttpGet("{employeeCode}", Name = "Salary_MultipleByID")]
        public ActionResult<List<SalaryDto>> Get([FromRoute] string employeeCode)
        {
            var salary = this._service.ListMultipleByID(employeeCode);
            var salaryDto = this._mapper.Map<List<SalaryDto>>(salary);

            return salaryDto;
        }

        [HttpGet]
        [Route("api/Salary/GetByPK/{Id}")]
        public ActionResult<SalaryDto> GetByPK([FromRoute] int Id)
        {
            var salary = this._service.ListById(Id);
            var salaryDto = this._mapper.Map<SalaryDto>(salary);

            return salaryDto;
        }

        [HttpPost]
        public ActionResult Post([FromBody] List<SalaryCreationDto> salaries)
        {

            var salary = this._mapper.Map<List<_Salary>>(salaries);
            this._service.Add(salary);

            var objDto = this._mapper.Map<List<SalaryDto>>(salary);
            return new CreatedAtRouteResult("Salary_MultipleByID", new { employeeCode = objDto[0].EmployeeCode }, salary);
        }
        
        [HttpPut]
        //public ActionResult Put([FromBody] List<SalaryModificationDto> salaries)
        public ActionResult<SalaryAux<_Salary>> Put([FromBody] List<SalaryModificationDto> salaries)
        {

            if (salaries == null) { return BadRequest("Debe proporicionar datos para insertar"); }
            var salary = this._mapper.Map<List<_Salary>>(salaries);
            var result = this._service.Edit(salary);
            //if(result == null) { return BadRequest("Duplicidad de registro no permitida"); }

            return result;
            //var objDto = this._mapper.Map<List<SalaryDto>>(salary);
            //return new CreatedAtRouteResult("Salary_MultipleByID", new { employeeCode = objDto[0].EmployeeCode }, salary);
        }

    }
}
