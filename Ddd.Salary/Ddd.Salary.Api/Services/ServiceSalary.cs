﻿using Ddd.Salary.Domain.Entities;
using Ddd.Salary.Domain.Repositories;
using Ddd.Salary.Api.Interfaces;
using System.Collections.Generic;
using Ddd.Salary.Domain.Helpers;
using Ddd.Salary.Infrastructure.Sql.Repositories;

namespace Ddd.Salary.Api.Services
{
    public class ServiceSalary : IServiceSalary<SalaryAux<_Salary>, _Salary, int, string>
    {
        private readonly IRepositorySalary<SalaryAux<_Salary>, _Salary, int, string> _repositorySalary;

        public ServiceSalary(IRepositorySalary<SalaryAux<_Salary>, _Salary, int, string> repositorySalary)
        {
            this._repositorySalary = repositorySalary;
        }


        public SalaryAux<_Salary> Add(List<_Salary> entity)
        {
            var result = this._repositorySalary.Add(entity);
            this._repositorySalary.SaveChanges();
            return result;
        }

        public SalaryAux<_Salary> Edit(List<_Salary> entity)
        {
            var result = this._repositorySalary.Edit(entity);
            this._repositorySalary.SaveChanges();
            return result;
        }

        public List<_Salary> List()
        {
            return this._repositorySalary.List();
        }

        public _Salary ListById(int entityPK)
        {
            return this._repositorySalary.ListById(entityPK);
        }

        public List<_Salary> ListMultipleByID(string entityID)
        {
            return this._repositorySalary.ListMultipleByID(entityID);
        }
    }
}
