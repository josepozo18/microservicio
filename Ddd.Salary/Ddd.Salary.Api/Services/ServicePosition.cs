﻿using Ddd.Salary.Api.Interfaces;
using Ddd.Salary.Domain.Entities;
using Ddd.Salary.Domain.Repositories;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Ddd.Salary.Api.Services
{
    public class ServicePosition : IServicePosition<Position, int, string>
    {
        private readonly IRepositoryPosition<Position, int, string> _repositoryPosition;

        public ServicePosition(IRepositoryPosition<Position, int, string> repositoryPosition)
        {
            this._repositoryPosition = repositoryPosition;
        }

        public List<Position> List()
        {
            return this._repositoryPosition.List();
        }

        public Position ListById(int entityPK)
        {
            throw new NotImplementedException();
        }

        public List<Position> ListMultipleByID(int entityID)
        {
            throw new NotImplementedException();
        }

        public List<Position> ListMultipleByID(string entityID)
        {
            throw new NotImplementedException();
        }
    }
}
