﻿using Ddd.Salary.Api.Interfaces;
using Ddd.Salary.Domain.Entities;
using Ddd.Salary.Domain.Repositories;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Ddd.Salary.Api.Services
{
    public class ServiceDivision : IServiceOffice<Division, int, string>
    {
        private readonly IRepositoryDivision<Division, int, string> _repositoryDivision;

        public ServiceDivision(IRepositoryDivision<Division, int, string> repositoryDivision)
        {
            this._repositoryDivision = repositoryDivision;
        }

        public List<Division> List()
        {
            return this._repositoryDivision.List();
        }

        public Division ListById(int entityPK)
        {
            throw new NotImplementedException();
        }

        public List<Division> ListMultipleByID(int entityID)
        {
            throw new NotImplementedException();
        }

        public List<Division> ListMultipleByID(string entityID)
        {
            throw new NotImplementedException();
        }
    }
}
