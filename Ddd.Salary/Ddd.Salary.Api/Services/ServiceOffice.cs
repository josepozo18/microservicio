﻿using Ddd.Salary.Api.Interfaces;
using Ddd.Salary.Domain.Entities;
using Ddd.Salary.Domain.Repositories;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Ddd.Salary.Api.Services
{
    public class ServiceOffice : IServiceOffice<Office, int, string>
    {
        private readonly IRepositoryOffice<Office, int, string> _repositoryOffice;

        public ServiceOffice(IRepositoryOffice<Office, int, string> repositoryOffice)
        {
            this._repositoryOffice = repositoryOffice;
        }

        public List<Office> List()
        {
            return this._repositoryOffice.List();
        }

        public Office ListById(int entityPK)
        {
            throw new NotImplementedException();
        }

        public List<Office> ListMultipleByID(string entityID)
        {
            throw new NotImplementedException();
        }
    }
}
