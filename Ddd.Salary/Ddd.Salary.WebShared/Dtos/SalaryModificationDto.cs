﻿using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Ddd.Salary.WebShared.Dtos
{
    public class SalaryModificationDto
    {
        public int Id { get; set; }
        [Required(ErrorMessage = "The field 'Year' is required!")]
        public int Year { get; set; }
        [Required(ErrorMessage = "The field 'Month' is required!")]
        public int Month { get; set; }
        public int OfficeId { get; set; }
        [Required(ErrorMessage = "the field 'Employee Code' is required!")]
        [StringLength(10)]
        public string EmployeeCode { get; set; }
        [Required(ErrorMessage = "the field 'Name' is required!")]
        [StringLength(150)]
        public string Name { get; set; }
        [Required(ErrorMessage = "the field 'Surname' is required!")]
        [StringLength(150)]
        public string Surname { get; set; }
        public int DivisionId { get; set; }
        public int PositionId { get; set; }
        public int Grade { get; set; }
        [DataType(DataType.Date)]
        public DateTime BeginDate { get; set; }
        [DataType(DataType.Date)]
        public DateTime BirthDay { get; set; }
        [StringLength(10)]
        public string IdentificationNumber { get; set; }
        [Column(TypeName = "decimal(18,2)")]
        public decimal BaseSalary { get; set; }
        [Column(TypeName = "decimal(18,2)")]
        public decimal ProductionBonus { get; set; }
        [Column(TypeName = "decimal(18,2)")]
        public decimal CompensationBonus { get; set; }
        [Column(TypeName = "decimal(18,2)")]
        public decimal Comission { get; set; }
        [Column(TypeName = "decimal(18,2)")]
        public decimal Contributions { get; set; }
    }
}
