﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Ddd.Salary.WebShared.Dtos
{
    public class DivisionDto
    {
        public int Id { get; set; }
        public string Nombre { get; set; }
    }
}
