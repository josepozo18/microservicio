﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Ddd.Salary.WebShared.Dtos
{
    public class SalaryDto
    {
        public int Id { get; set; }
        public int Year { get; set; }
        public int Month { get; set; }
        public int OfficeId { get; set; }
        public string EmployeeCode { get; set; }
        public string Name { get; set; }
        public string Surname { get; set; }
        public int DivisionId { get; set; }
        public int PositionId { get; set; }
        public int Grade { get; set; }
        public DateTime BeginDate { get; set; }
        public DateTime BirthDay { get; set; }
        public string IdentificationNumber { get; set; }
        public decimal BaseSalary { get; set; }
        public decimal ProductionBonus { get; set; }
        public decimal CompensationBonus { get; set; }
        public decimal Comission { get; set; }
        public decimal Contributions { get; set; }

        public decimal TotalSalary { get; set; }

        public OfficeDto Office { get; set; }
        public DivisionDto Division { get; set; }
        public PositionDto Position { get; set; }
    }
}
