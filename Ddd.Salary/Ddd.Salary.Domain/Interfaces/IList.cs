﻿using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Ddd.Salary.Domain.Interfaces
{
    public interface IList<TEntity, TEntityPK, TEntityID>
    {
        List<TEntity> List();

        TEntity ListById(TEntityPK entityPK);
        List<TEntity> ListMultipleByID(TEntityID entityID);

    }
}
