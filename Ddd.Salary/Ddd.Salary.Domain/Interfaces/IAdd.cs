﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Ddd.Salary.Domain.Interfaces
{
    public interface IAdd<TEntidadAux, TEntity>
    {
        TEntidadAux Add(List<TEntity> entity);
    }
}
