﻿using System.Collections.Generic;

namespace Ddd.Salary.Domain.Helpers
{
    public class SalaryAux<T>
    {
        public List<T> Data { get; set; }
        public string Message { get; set; }
    }
}
