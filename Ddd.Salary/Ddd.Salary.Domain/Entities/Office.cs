﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Ddd.Salary.Domain.Entities
{
    [Table("Office", Schema = "dbo")]
    public class Office
    {
        [Key]
        public int Id { get; set; }
        [Required(ErrorMessage = "Name of office is required!")]
        [StringLength(50)]
        public string Nombre { get; set; }
    }
}
