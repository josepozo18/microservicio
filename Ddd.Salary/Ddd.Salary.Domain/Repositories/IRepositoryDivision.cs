﻿using Ddd.Salary.Domain.Interfaces;

namespace Ddd.Salary.Domain.Repositories
{
    public interface IRepositoryDivision<TEntity, TEntidyPK, TEntityID>
        : IList<TEntity, TEntidyPK, TEntityID>
    {
    }
}
