﻿using System;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Ddd.Salary.Domain.Interfaces;

namespace Ddd.Salary.Domain.Repositories
{
    public interface IRepositoryPosition<TEntidad, TEntidyPK, TEntidadID>
        : IList<TEntidad, TEntidyPK, TEntidadID>
    {
    }
}
