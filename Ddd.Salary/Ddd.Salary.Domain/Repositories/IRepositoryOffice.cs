﻿using Ddd.Salary.Domain.Interfaces;

namespace Ddd.Salary.Domain.Repositories
{
    public interface IRepositoryOffice<TEntity, TEntidyPK, TEntityID>
        : IList<TEntity, TEntidyPK, TEntityID>
    {
    }
}
