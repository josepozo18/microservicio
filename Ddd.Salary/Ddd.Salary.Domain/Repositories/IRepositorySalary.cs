﻿using Ddd.Salary.Domain.Interfaces;

namespace Ddd.Salary.Domain.Repositories
{
    public interface IRepositorySalary<TEntityAux, TEntity, TEntityPK, TEntityID>
        : IAdd<TEntityAux, TEntity>, IEdit<TEntityAux, TEntity>, IList<TEntity, TEntityPK, TEntityID>, ITransaction<TEntity>
    {
    }
}
